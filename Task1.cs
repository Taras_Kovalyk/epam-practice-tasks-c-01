using System;

class Vector
{		
	private int _x;
	private int _y;
	private int _z;
	private double _vectorLength;
	
	Vector(int xA, int yA, int zA, int xB, int yB, int zB)
	{		
		_x = xB - xA;
		_y = yB - yA;
		_z = zB - zA;
		_vectorLength = Math.Sqrt(_x * _x + _y * _y + _z * _z);
	}
	
	Vector(int x, int y, int z)
	{
		_x = x;
		_y = y;
		_z = z;
		_vectorLength = Math.Sqrt(_x * _x + _y * _y + _z * _z);
	}
	
	public static Vector operator + (Vector v1, Vector v2)
	{
		return new Vector(v1._x + v2._x, v1._y + v2._y, v1._z + v2._z);
	}
	
	public static Vector operator - (Vector v1, Vector v2)
	{
		return new Vector(v1._x - v2._x, v1._y - v2._y, v1._z - v2._z);
	}
	
	public static bool operator > (Vector v1, Vector v2)
	{
		if(v1._vectorLength > v2._vectorLength)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator >= (Vector v1, Vector v2)
	{
		if(v1._vectorLength >= v2._vectorLength)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator <= (Vector v1, Vector v2)
	{
		if(v1._vectorLength <= v2._vectorLength)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator < (Vector v1, Vector v2)
	{
		if(v1._vectorLength < v2._vectorLength)
		{
			return true;
		}	
		else
		{
			return false;
		}
	}
	
	public static bool operator == (Vector v1, Vector v2)
	{
		if(v1._vectorLength == v2._vectorLength)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static bool operator != (Vector v1, Vector v2)
	{
		if(v1._vectorLength != v2._vectorLength)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static int CalculateScalarMultiplication(Vector v1, Vector v2)
	{
		return v1._x * v2._x + v1._y * v2._y + v1._z * v2._z;
	}
	
	public static Vector CalculateVectorMultiplication(Vector v1, Vector v2)
	{
		return new Vector(v1._y * v2._z - v1._z * v2._y, v1._z * v2._x - v1._x * v2._z, v1._x * v2._y - v1._y * v2._x);
	}
	
	public static double CalculateMixedMultiplication(Vector v1, Vector v2, Vector v3)
	{
		return CalculateScalarMultiplication(CalculateVectorMultiplication(v1, v2), v3);
	}
	
	public static double FindAngle(Vector v1, Vector v2)
	{
		return CalculateScalarMultiplication(v1, v2) / (v1._vectorLength * v2._vectorLength);
	}	
	
	static void Main()
	{
		var vector1 = new Vector(1, 3, 4, 5, 7, 1);
		var vector2 = new Vector(5, 2, 1, 9, 2, 2);
		var vector3 = new Vector(8, 2, 5, 2, 1, 4);
		Console.WriteLine("We have 3 vectors. First one is ({0};{1};{2}), second ({3},{4},{5}), third ({6},{7},{8}).", vector1._x, vector1._y, vector1._z,
			vector2._x, vector2._y, vector2._z, vector3._x, vector3._y, vector3._z);
		Console.WriteLine("Length of first is {0}, second is {1}, third {2}", Math.Round(vector1._vectorLength, 2), Math.Round(vector2._vectorLength, 2),
			Math.Round(vector3._vectorLength, 2));
			
		var testSum = vector1 + vector2;
		
		Console.WriteLine("Sum of vector1 and vector2 is ({0},{1},{2}).", testSum._x, testSum._y, testSum._z);
		
		var testSub = vector1 - vector2;
		
		Console.WriteLine("Sub of vector1 and vector2 is ({0},{1},{2}).", testSub._x, testSub._y, testSub._z);
		
		int scalarResult = CalculateScalarMultiplication(vector1, vector2);
		
		Console.WriteLine("Scalar multiplication of vector1 and vector2 is {0}.", scalarResult);
		
		var vectorMult = CalculateVectorMultiplication(vector1, vector2);
		
		Console.WriteLine("Vector multiplication of vector1 and vector2 is ({0},{1},{2}).", vectorMult._x, vectorMult._y, vectorMult._z);
		
		double mixedMult = CalculateMixedMultiplication(vector1, vector2, vector3);
		
		Console.WriteLine("Mixed multiplication of vector1, vector2 and vector3 is {0}.", Math.Round(mixedMult, 2));

		double vecAngle = FindAngle(vector1, vector2);
		
		Console.WriteLine("Angle between vector1 and vector2 is {0}", Math.Round(vecAngle, 2));
		
		if(vector1 > vector2)
		{
			Console.WriteLine("vector1 is bigger than vector2");
		}
		else if(vector1 < vector2)
		{
			Console.WriteLine("vector1 is smaller than vector2");
		}
		
		if(vector1 == vector2)
		{
			Console.WriteLine("vector1 and vector2 are equal");
		}
		else if(vector1 != vector2)
		{
			Console.WriteLine("vector1 and vector2 are not equal");
		}			
		
		Console.ReadLine();
	}
	
	
}